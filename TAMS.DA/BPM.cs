﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace TAMS.DA
{
    public class BPM
    {
        private const int CMD_TIMEOUT = 36000;
        public string GetInternalPrefixFromDatabase()
        {
            Database db         = DatabaseFactory.CreateDatabase("POSDatabase");
            DbCommand cmd       = db.GetStoredProcCommand("IntService_get_AppSetting");            
            cmd.CommandTimeout  = CMD_TIMEOUT;
            DataTable result    = db.ExecuteDataSet(cmd).Tables[0];   
            string dbPrefix     = null;

            foreach (DataRow r in result.Rows)
            {
                dbPrefix = r["SettingValue"].ToString();
            }

            return dbPrefix;
        }
    }
}
