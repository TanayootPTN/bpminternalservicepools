﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BPM.BS;
using BPM.Model;
using TAMS.BS;
using TAMS.Model;


namespace BPMInternalServicePools
{
    /// <summary>
    /// Summary description for ServicePools
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ServicePools : System.Web.Services.WebService
    {
        [WebMethod]
        public bool ConfirmServicePrefix(string notification)
        {
            var bs      = new BPM.BS.Services();
            var result  = bs.ConfirmPrefixCanUseInternalService(notification);
            return result;
        }

        [WebMethod]
        public List<BPM.Model.ARInfoModel> GetARByNotification(string notification)
        {
            var result  = new List<BPM.Model.ARInfoModel>();
            var bpm     = new BPM.BS.Services();
            var prefix  = bpm.GetPrefixFromNotification(notification);

            if (prefix.ToUpper() == "TA")
            {
                var tams    = new TAMS.BS.Services();
                result      = tams.GetARByNotification(notification);
            }
            else { }

            return result;
        }

        [WebMethod]
        public bool UpdatePayment(OneTouchLogInfo info)
        {
            var bpm     = new BPM.BS.Services();
            var prefix  = bpm.GetPrefixFromNotification(info.NotificationNo.Trim());

            if (prefix.ToUpper() == "TA")   //Update TAMS 
            {
                var tams = new TAMS.BS.Services();
                tams.UpdatePayment(info);
            }
            else { }
           
            return true;
        }


    }
}
