﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPM.BS
{
    public class Services
    {
        #region ..Notification Prefix Verification        
        public bool ConfirmPrefixCanUseInternalService(string notificationNumber)
        {
            bool isCanUse = false;
            if (String.IsNullOrEmpty(notificationNumber) || String.IsNullOrWhiteSpace(notificationNumber))
            {
                isCanUse = false;
            }
            else
            {
                var targetPrefix = GetPrefixFromNotification(notificationNumber);
                var listPrefix = GetAvailablePrefixs();
                foreach (var p in listPrefix)
                {
                    if (p.ToUpper() == targetPrefix.ToUpper())
                    {
                        isCanUse = true;
                    }
                }
            }
            return isCanUse;
        }
        public string GetPrefixFromNotification(string notificationNumber)
        {
            string result = null;
            try
            {
                result = notificationNumber.Substring(0, 2);
            }
            catch { }
            return result;
        }
        private List<string> GetAvailablePrefixs()
        {
            var da = new BPM.DA.Config();
            string dbPrefix = da.GetInternalPrefixFromDatabase();
            var prefixs = new List<string>();
            foreach (var s in dbPrefix.Split(','))
            {
                prefixs.Add(s);
            }

            return prefixs;
        }       
        #endregion
    }
}
